#!/usr/bin/env python3
import urllib.request
import json
import csv
import sys

import ssl
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

if len(sys.argv) < 2:
    HOSTPORT = "localhost:5001"
else:
    HOSTPORT = sys.argv[1]

HEADERS = [('Content-Type', 'application/json')]
URL = f"https://{HOSTPORT}/Api/Authorization/Login"

writer = csv.writer(sys.stdout)

for i in range(1000):
    data = json.dumps({
        "userName":f"u{i}",
        "password":"12345"
    })
    req = urllib.request.Request(URL, data=data.encode('ascii'))
    for h, v in HEADERS:
        req.add_header(h, v)
    with urllib.request.urlopen(req, context=ctx) as response:
        token = json.loads(response.read())["token"]
        writer.writerow((token,))
