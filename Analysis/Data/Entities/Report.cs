﻿using System;

namespace Analysis.Data.Entities
{
    public class Report
    {
        public Guid Id { get; set; }
        public DateTime Date { get; set; }
        public double PercentageOfPlagiarism { get; set; }
    }
}
