﻿using Analysis.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace Analysis.Data
{
    public class ReportsContext : DbContext
    {
        public ReportsContext(DbContextOptions<ReportsContext> options) : base(options)
        {
        }

        public DbSet<Report> Reports { get; set; }
        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=reports_db;Username=postgres;Password=1234");
        //}
    }
}
