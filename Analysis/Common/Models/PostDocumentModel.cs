using System;
using System.ComponentModel.DataAnnotations;

namespace Dataset.Common.Models
{
    public class PostDocumentModel
    {
        [Required] public string Name { get; set; }

        [Required] public Guid Author { get; set; }

        [Required] public byte[] Content { get; set; }
    }
}