﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Analysis.Common.Models
{
    public class FileModel
    {
        [Required] public Guid Id { get; set; }

        [Required] public string Name { get; set; }
    }
}
