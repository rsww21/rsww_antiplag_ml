﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Analysis.Common.Models
{
    public enum StatusEnum
    {
        NotStarted,
        InProgress,
        Completed
    }

    public class StatusModel
    {
        [Required] public FileModel File { get; set; }

        [Required] public string Status { get; set; }

        [Required] public DateTime? StartDate { get; set; }
    }
}
