﻿using System;

namespace Analysis.Common.Models
{
    public class ReportModel
    {
        public Guid Id { get; set; }
        public Guid AuthorId { get; set; }
        public string FileName { get; set; }
        public DateTime Date { get; set; }
        public double PercentageOfPlagiarism { get; set; }
        public string details { get; set; }
    }
}
