﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Analysis.Common.Models
{
    public class UserFileModel
    {
        [Required] public Guid UserId { get; set; }

        [Required] public Guid DocumentId { get; set; }
    }
}
