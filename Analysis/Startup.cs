using Analysis.Data;
using Dataset.Common;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System.Collections.Generic;
using Analysis.Messaging.Configuration;
using Analysis.Messaging.Receivers;
using Analysis.Messaging.Senders;
using Microsoft.Extensions.Options;
using System.Threading;
using Analysis.Common;
using Analysis.Hubs;

namespace Analysis
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
	    ThreadPool.SetMinThreads(250,250);
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<RabbitMqConfiguration>(Configuration.GetSection("RabbitMq"));
            services.AddSingleton(sp => sp.GetRequiredService<IOptions<RabbitMqConfiguration>>().Value);

            services.AddControllers();

            services.AddSignalR().AddRedis(Configuration["Redis:ConnectionString"], options =>
            {
                options.Configuration.DefaultDatabase = 1;
            });
            services.AddDbContext<ReportsContext>(options =>
                    options.UseNpgsql(Configuration.GetConnectionString("ReportsContext")), ServiceLifetime.Transient,
                ServiceLifetime.Singleton);

            services.AddSwaggerGen(c =>
            {
                c.AddSecurityDefinition("JWT", new OpenApiSecurityScheme
                {
                    Type = SecuritySchemeType.Http,
                    Scheme = "Bearer",
                    BearerFormat = "JWT"
                });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference {Type = ReferenceType.SecurityScheme, Id = "JWT"}
                        },
                        new List<string>()
                    }
                });

                c.SwaggerDoc("v1", new OpenApiInfo {Title = "Analysis", Version = "v1"});
            });
            services.AddMediatR(typeof(Startup));
            services.AddSingleton<IRedisConnection, RedisConnection>();
            services.AddSingleton<IAnalysisFileSender, AnalysisFileSender>();

            services.AddHostedService<ReportsReceiver>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger(c => { c.RouteTemplate = "Api/Analysis/swagger/{documentName}/swagger.json"; });

                using var scope = app.ApplicationServices.CreateScope();
                var dbContext = scope.ServiceProvider.GetService<ReportsContext>();
                dbContext?.Database.Migrate();
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHub<ReportsHub>("/hubs/reports");
                endpoints.MapControllers();
            });
        }
    }
}
