﻿using Analysis.Common.Models;
using Analysis.Data;
using Dataset.Common;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Analysis.Common;

namespace Analysis.Queries.GetReports
{
    public class GetStatusesQuery : IRequest<GetStatusesResponse>
    {
        private readonly Guid UserId;

        public GetStatusesQuery(Guid userId)
        {
            UserId = userId;
        }

        public class GetStatusesQueryHandler : IRequestHandler<GetStatusesQuery, GetStatusesResponse>
        {
            private readonly IRedisConnection redis;
            private readonly ReportsContext reportsContext;

            public GetStatusesQueryHandler(IRedisConnection redis, ReportsContext reportsContext)
            {
                this.redis = redis;
                this.reportsContext = reportsContext;
            }

            public async Task<GetStatusesResponse> Handle(GetStatusesQuery request, CancellationToken cancellationToken)
            {
                var database = redis.GetDatabase();
                var keys = database.ListRangeAsync(request.UserId.ToString());
                var statuses = new List<StatusModel>();
                foreach (var key in await keys)
                {
                    var report = reportsContext.Reports.SingleOrDefault(r => r.Id.ToString() == key.ToString());
                    StatusEnum status = report != null ? StatusEnum.Completed : StatusEnum.InProgress;
                    statuses.Add(new()
                    {
                        File = new FileModel() { Id = new Guid(key.ToString()), Name = await database.StringGetAsync(key.ToString()) },
                        Status = status.ToString(),
                        StartDate = report?.Date.ToUniversalTime()
                    });
                }
                return new GetStatusesResponse() { Statuses = statuses };
            }
        }
    }
}
