﻿using Analysis.Common.Models;
using System.Collections.Generic;

namespace Analysis.Queries.GetReports
{
    public class GetStatusesResponse
    {
        public List<StatusModel> Statuses { get; set; }
    }
}
