﻿using Analysis.Common.Models;
using System.Collections.Generic;

namespace Analysis.Queries.GetReports
{
    public class GetReportsResponse
    {
        public List<ReportModel> Reports { get; set; }
        public GetReportsResponse(List<ReportModel> reports)
        {
            Reports = reports;
        }
    }
}
