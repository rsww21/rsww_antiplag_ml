using Analysis.Common.Models;
using Analysis.Data;
using Dataset.Common;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Analysis.Common;

namespace Analysis.Queries.GetReports
{
    public class GetReportsQuery : IRequest<GetReportsResponse>
    {
        private readonly Guid UserId;

        public GetReportsQuery(Guid userId)
        {
            UserId = userId;
        }

        public class GetReportsQueryHandler : IRequestHandler<GetReportsQuery, GetReportsResponse>
        {
            private readonly IRedisConnection redis;
            private readonly ReportsContext reportsContext;

            public GetReportsQueryHandler(IRedisConnection redis, ReportsContext reportsContext)
            {
                this.redis = redis;
                this.reportsContext = reportsContext;
            }

            public async Task<GetReportsResponse> Handle(GetReportsQuery request, CancellationToken cancellationToken)
            {
                var database = redis.GetDatabase();
                var keys = database.ListRangeAsync(request.UserId.ToString());
                var reports = new List<ReportModel>();
                foreach (var key in await keys)
                {
                    var report = reportsContext.Reports.SingleOrDefault(r => r.Id.ToString() == key.ToString());
                    if (report != null)
                    {
                        var rdetails = database.StringGet(report.Id.ToString()+":details");  
                        reports.Add(new()
                        {
                            Id = report.Id,
                            FileName = database.StringGet(key.ToString()),
                            AuthorId = request.UserId,
                            Date = report.Date.ToUniversalTime(),
                            PercentageOfPlagiarism = report.PercentageOfPlagiarism,
                            details = rdetails.HasValue ? rdetails.ToString() : ""
                        });
                    }
                }

                return new GetReportsResponse(reports);
            }
        }
    }
}
