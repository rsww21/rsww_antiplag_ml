﻿using Analysis.Common.Models;
using Dataset.Common;
using MediatR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Analysis.Common;

namespace Analysis.Queries.GetReports
{
    public class GetFilesQuery : IRequest<GetFilesResponse>
    {
        private readonly Guid UserId;

        public GetFilesQuery(Guid userId)
        {
            UserId = userId;
        }

        public class GetFilesQueryHandler : IRequestHandler<GetFilesQuery, GetFilesResponse>
        {
            private readonly IRedisConnection redis;

            public GetFilesQueryHandler(IRedisConnection redis)
            {
                this.redis = redis;
            }

            public async Task<GetFilesResponse> Handle(GetFilesQuery request, CancellationToken cancellationToken)
            {
                var database = redis.GetDatabase();
                var keys = database.ListRangeAsync(request.UserId.ToString());
                var files = new List<FileModel>();
                foreach (var key in await keys)
                {
                    files.Add(new()
                    {
                        Id = new Guid(key.ToString()),
                        Name = await database.StringGetAsync(key.ToString())
                    });
                }
                return new GetFilesResponse() { Files = files };
            }
        }
    }
}
