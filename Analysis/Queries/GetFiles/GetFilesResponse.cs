﻿using Analysis.Common.Models;
using System.Collections.Generic;

namespace Analysis.Queries.GetReports
{
    public class GetFilesResponse
    {
        public List<FileModel> Files { get; set; }
    }
}
