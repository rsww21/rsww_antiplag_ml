﻿using Analysis.Common.Exceptions;
using Analysis.Common.Models;
using Analysis.Data;
using Dataset.Common;
using MediatR;
using StackExchange.Redis;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Analysis.Common;

namespace Analysis.Queries.GetReports
{
    public class GetReportQuery : IRequest<GetReportResponse>
    {
        private readonly UserFileModel model;

        public GetReportQuery(UserFileModel model)
        {
            this.model = model;
        }

        public class GetReportsQueryHandler : IRequestHandler<GetReportQuery, GetReportResponse>
        {

            private readonly IRedisConnection redis;
            private readonly ReportsContext reportsContext;

            public GetReportsQueryHandler(IRedisConnection redis, ReportsContext reportsContext)
            {
                this.redis = redis;
                this.reportsContext = reportsContext;
            }

            public async Task<GetReportResponse> Handle(GetReportQuery request, CancellationToken cancellationToken)
            {
                var database = redis.GetDatabase();
                var keys = await database.ListRangeAsync(request.model.UserId.ToString());
                var result = keys.SingleOrDefault(k => k.ToString() == request.model.DocumentId.ToString());
                if (result != RedisValue.Null)
                {
                    var report = reportsContext.Reports.SingleOrDefault(r => r.Id == request.model.DocumentId);
                    if (report != null)
                    {
                        ReportModel model = new()
                        {
                            Id = report.Id,
                            FileName = database.StringGet(result.ToString()),
                            AuthorId = request.model.UserId,
                            Date = report.Date,
                            PercentageOfPlagiarism = report.PercentageOfPlagiarism
                        };
                        return new GetReportResponse(model);
                    }
                }
                throw new FileNotExistException();
            }
        }
    }
}
