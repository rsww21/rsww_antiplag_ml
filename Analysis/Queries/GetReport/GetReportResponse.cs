﻿using Analysis.Common.Models;

namespace Analysis.Queries.GetReports
{
    public class GetReportResponse
    {
        public ReportModel Report { get; set; }
        public GetReportResponse(ReportModel report)
        {
            Report = report;
        }
    }
}
