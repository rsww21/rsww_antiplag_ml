using Dataset.Common.Models;
using System;

namespace Analysis.Messaging.Senders
{
    public interface IAnalysisFileSender
    {
        void SendFileToAnalysis(Guid docId, Guid userId);
        void SendFile(Guid docId, byte[] content);
    }
}