using System;
using System.Text;
using Analysis.Messaging.Configuration;
using Newtonsoft.Json;
using RabbitMQ.Client;
using System.Linq;

namespace Analysis.Messaging.Senders
{
    public class AnalysisFileSender : IAnalysisFileSender
    {
        private readonly RabbitMqConfiguration rabbitMqConfig;
        private IConnection connection;

        public AnalysisFileSender(RabbitMqConfiguration rabbitMqConfig)
        {
            this.rabbitMqConfig = rabbitMqConfig;
        }

        public void SendFileToAnalysis(Guid docId, Guid userId)
        {
            if (!ConnectionExists()) return;

            using var channel = connection.CreateModel();
            channel.QueueDeclare(rabbitMqConfig.AnalysisQueueName, false, false, false, null);
            var props = channel.CreateBasicProperties();

            props.CorrelationId = docId.ToString();
            props.ContentType = "application/json";

            var json = JsonConvert.SerializeObject(userId);
            var body = Encoding.UTF8.GetBytes(json);

            channel.BasicPublish("", rabbitMqConfig.AnalysisQueueName, props, body);
        }

        public void SendFile(Guid docId, byte[] content)
        {
            if (!ConnectionExists()) return;

            using var channel = connection.CreateModel();
            channel.QueueDeclare(rabbitMqConfig.AnalysisQueueName, false, false, false, null);
            var props = channel.CreateBasicProperties();

            props.CorrelationId = docId.ToString();
            props.ContentType = "application/octet-stream";

            var body = Encoding.UTF8.GetBytes("analysis\0")
                .Concat(content)
                .ToArray();

            channel.BasicPublish("", rabbitMqConfig.AnalysisQueueName, props, body);
        }

        private void CreateConnection()
        {
            var factory = new ConnectionFactory
            {
                HostName = rabbitMqConfig.Hostname,
                UserName = rabbitMqConfig.UserName,
                Password = rabbitMqConfig.Password
            };
            connection = factory.CreateConnection();
        }

        private bool ConnectionExists()
        {
            if (connection != null)
            {
                return true;
            }

            CreateConnection();

            return connection != null;
        }
    }
}