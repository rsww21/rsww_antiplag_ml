using System;

namespace Analysis.Messaging.Messages
{
    public class Report
    {
        public Guid Id { get; set; }
        public Guid AuthorId { get; set; }
        public DateTime Date { get; set; }
        public double PercentageOfPlagiarism { get; set; }
        public string details { get; set; }
    }
}
