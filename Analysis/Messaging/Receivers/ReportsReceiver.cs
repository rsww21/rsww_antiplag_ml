using System;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Analysis.Data;
using Analysis.Data.Entities;
using Analysis.Hubs;
using Analysis.Common;
using Analysis.Messaging.Configuration;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using StackExchange.Redis;

namespace Analysis.Messaging.Receivers
{
    public class ReportsReceiver : BackgroundService
    {
        private readonly RabbitMqConfiguration rabbitMqConfig;
        private readonly ReportsContext reportsContext;
        private readonly IHubContext<ReportsHub> reportsHubContext;
        private IModel channel;
        private IConnection connection;
        private readonly IRedisConnection redis;

        public ReportsReceiver(RabbitMqConfiguration rabbitMqConfig, ReportsContext reportsContext, IHubContext<ReportsHub> reportsHubContext, IRedisConnection redis)
        {
            this.rabbitMqConfig = rabbitMqConfig;
            this.reportsContext = reportsContext;
            this.reportsHubContext = reportsHubContext;
            this.redis = redis;
            InitializeRabbitMqListener();
        }

        private void InitializeRabbitMqListener()
        {
            var factory = new ConnectionFactory
            {
                HostName = rabbitMqConfig.Hostname,
                UserName = rabbitMqConfig.UserName,
                Password = rabbitMqConfig.Password
            };

            connection = factory.CreateConnection();
            connection.ConnectionShutdown += RabbitMQ_ConnectionShutdown;
            channel = connection.CreateModel();
            channel.QueueDeclare(rabbitMqConfig.ReportsQueueName, false, false, false, null);
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            stoppingToken.ThrowIfCancellationRequested();

            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (ch, ea) =>
            {
                var content = Encoding.UTF8.GetString(ea.Body.ToArray());
                var report = JsonConvert.DeserializeObject<Messages.Report>(content);

                HandleMessage(report);

                channel.BasicAck(ea.DeliveryTag, false);
            };
            consumer.Shutdown += OnConsumerShutdown;
            consumer.Registered += OnConsumerRegistered;
            consumer.Unregistered += OnConsumerUnregistered;
            consumer.ConsumerCancelled += OnConsumerConsumerCancelled;

            channel.BasicConsume(rabbitMqConfig.ReportsQueueName, false, consumer);

            return Task.CompletedTask;
        }

        private void HandleMessage(Messages.Report receivedReport) // todo: move this to other class
        {
            var report = reportsContext.Reports.SingleOrDefault(r => r.Id == receivedReport.Id);
            if (report != null)
            {
                reportsContext.Remove(report);
            }
            IDatabase database = redis.GetDatabase();

            reportsHubContext.Clients.Group(receivedReport.AuthorId.ToString())
                .SendAsync("Notify", receivedReport.PercentageOfPlagiarism);

            report = new Report
            {
                Id = receivedReport.Id,
                Date = receivedReport.Date,
                PercentageOfPlagiarism = receivedReport.PercentageOfPlagiarism
            };
            reportsContext.Add(report);
            reportsContext.SaveChanges();
            database.StringSet(receivedReport.Id.ToString()+":details", receivedReport.details);
        }

        private void OnConsumerConsumerCancelled(object sender, ConsumerEventArgs e)
        {
        }

        private void OnConsumerUnregistered(object sender, ConsumerEventArgs e)
        {
        }

        private void OnConsumerRegistered(object sender, ConsumerEventArgs e)
        {
        }

        private void OnConsumerShutdown(object sender, ShutdownEventArgs e)
        {
        }

        private void RabbitMQ_ConnectionShutdown(object sender, ShutdownEventArgs e)
        {
        }

        public override void Dispose()
        {
            channel.Close();
            connection.Close();
            base.Dispose();
        }
    }
}
