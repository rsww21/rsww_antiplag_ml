namespace Dataset.Commands.PostDocument
{
    public class PostDocumentResponse
    {
        public PostDocumentResponse(string id, string filename)
        {
            Id = id;
            Name = filename;
        }

        public string Id { get; }
        public string Name { get; }
    }
}