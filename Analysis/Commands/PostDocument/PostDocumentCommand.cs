using Dataset.Common;
using Dataset.Common.Models;
using MediatR;
using StackExchange.Redis;
using System;
using System.Threading;
using System.Threading.Tasks;
using Analysis.Common;
using Analysis.Messaging.Senders;

namespace Dataset.Commands.PostDocument
{
    public class PostDocumentCommand : IRequest<PostDocumentResponse>
    {
        private readonly PostDocumentModel document;

        public PostDocumentCommand(PostDocumentModel document)
        {
            this.document = document;
        }

        public class PostDocumentCommandHandler : IRequestHandler<PostDocumentCommand, PostDocumentResponse>
        {
            private readonly IRedisConnection redis;
            private readonly IAnalysisFileSender fileSender;

            public PostDocumentCommandHandler(IRedisConnection redis, IAnalysisFileSender fileSender)
            {
                this.redis = redis;
                this.fileSender = fileSender;
            }

            public async Task<PostDocumentResponse> Handle(PostDocumentCommand request, CancellationToken cancellationToken)
            {
                IDatabase database = redis.GetDatabase();
                var key = Guid.NewGuid();
                var a1 = database.ListLeftPushAsync(request.document.Author.ToString(), key.ToString());
                var a2 = database.StringSetAsync(key.ToString(), request.document.Name);
                await Task.WhenAll(a1, a2);

                fileSender.SendFile(key, request.document.Content);
                return new PostDocumentResponse(key.ToString(), request.document.Name);
            }
        }
    }
}
