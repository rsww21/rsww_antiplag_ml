﻿using Analysis.Common.Exceptions;
using Analysis.Common.Models;
using MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Analysis.Common;
using Analysis.Messaging.Senders;

namespace Analysis.Commands.Analyse
{
    public class AnalyseCommand : IRequest<AnalyseCommandResponse>
    {
        private readonly UserFileModel model;

        public AnalyseCommand(UserFileModel model)
        {
            this.model = model;
        }

        public class AnalyseCommandHandler : IRequestHandler<AnalyseCommand, AnalyseCommandResponse>
        {
            private readonly IRedisConnection redis;
            private readonly IAnalysisFileSender fileSender;

            public AnalyseCommandHandler(IRedisConnection redis,
                IAnalysisFileSender fileSender)
            {
                this.redis = redis;
                this.fileSender = fileSender;
            }

            public async Task<AnalyseCommandResponse> Handle(AnalyseCommand request,
                CancellationToken cancellationToken)
            {
                var database = redis.GetDatabase();
                var keys = database.ListRangeAsync(request.model.UserId.ToString());
                var key = (await keys).SingleOrDefault(k =>
                    k.HasValue && k.ToString() == request.model.DocumentId.ToString());

                if (!key.HasValue) throw new FileNotExistException();

                fileSender.SendFileToAnalysis(request.model.DocumentId, request.model.UserId);

                return new AnalyseCommandResponse();

            }
        }
    }
}