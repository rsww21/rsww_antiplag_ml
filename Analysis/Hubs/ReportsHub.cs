using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace Analysis.Hubs
{

    public class ReportsHub : Hub
    {
        public override Task OnConnectedAsync()
        {
            var httpContext = Context.GetHttpContext();
            if (httpContext.Request.Query.TryGetValue("userId", out var userId))
            {
                Groups.AddToGroupAsync(Context.ConnectionId, userId);
            }

            return base.OnConnectedAsync();
        }
    }
}