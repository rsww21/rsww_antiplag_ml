﻿using Analysis.Commands.Analyse;
using Analysis.Common.Exceptions;
using Analysis.Common.Models;
using Analysis.Queries.GetReports;
using Dataset.Commands.PostDocument;
using Dataset.Common.Models;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Threading.Tasks;

namespace Analysis.Controllers
{
    [ApiController]
    [Route("Api/[controller]")]
    public class AnalysisController : ControllerBase
    {
        const long MAXFILESIZE = 28 * (1 << 20); // 28 MB
        private readonly IMediator mediator;

        public AnalysisController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpPost("files")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<PostDocumentResponse>> Post([FromHeader] Guid UserId, [FromForm, Required] string name, [Required] IFormFile file)
        {
            if (file.Length <= MAXFILESIZE)
            {
                using var memoryStream = new MemoryStream();
                await file.CopyToAsync(memoryStream);
                var model = new PostDocumentModel { Name = name, Author = UserId, Content = memoryStream.ToArray() };
                var documents = await mediator.Send(new PostDocumentCommand(model));
                return Ok(documents);
            }
            else
            {
                return BadRequest("File too large");
            }
        }

        [HttpPost("files/{id:Guid}/analyse")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<AnalyseCommandResponse>> Post([FromHeader] Guid UserId, [FromRoute] Guid id)
        {
            try
            {
                var model = new UserFileModel { UserId = UserId, DocumentId = id };
                var result = await mediator.Send(new AnalyseCommand(model));
                return Ok(result);
            }
            catch (FileNotExistException)
            {
                return NotFound("No such File");
            }
        }

        [HttpGet("files")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<GetFilesResponse>> Get([FromHeader] Guid userId)
        {
            var result = await mediator.Send(new GetFilesQuery(userId));
            return Ok(result);
        }

        [HttpGet("files/{id:Guid}/report")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<AnalyseCommandResponse>> Get([FromHeader] Guid UserId, [FromRoute] Guid id)
        {
            try
            {
                var model = new UserFileModel { UserId = UserId, DocumentId = id };
                var result = await mediator.Send(new GetReportQuery(model));
                return Ok(result);
            }
            catch (FileNotExistException)
            {
                return NotFound("No such File");
            }
        }

        [HttpGet("reports")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<GetReportsResponse>> GetReports([FromHeader] Guid userId)
        {
            var result = await mediator.Send(new GetReportsQuery(userId));
            return Ok(result);
        }

        [HttpGet("statuses")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<GetStatusesResponse>> GetStatuses([FromHeader] Guid userId)
        {
            var result = await mediator.Send(new GetStatusesQuery(userId));
            return Ok(result);
        }
    }
}
