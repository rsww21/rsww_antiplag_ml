from urllib.request import urlretrieve
import gzip
import csv
import os.path
import zipfile
from io import BytesIO
import datetime
import sys

import scrapy
from scrapy.spidermiddlewares.httperror import HttpError

from .pdftext import pdf_get_str

PDFSZIPLIMIT = 1024**3 * 30
TEXTSIZELIMIT = 1024**3 * 8

URLBASE = 'https://arxiv-dataset.storage.googleapis.com'

LISTCSVGZ = 'cs.LG_ids_presorted.csv.gz'
LISTURL = 'https://gitlab.com/rsww21/rsww_antiplag_ml/uploads/61629313867069151c96cb73301e5872/cs.LG_ids_presorted.csv.gz'

OUTARCHIVE = 'pdfs.zip'
OUTTEXTARCHIVE = 'text.zip'

def versions_load(verstr):
    return [ ver.split(':', 1) for ver in verstr.split(';') ]



class BasicSpider(scrapy.Spider):
    name = 'basic'
    allowed_domains = ['arxiv-dataset.storage.googleapis.com', 'export.arxiv.org']

    pdfslimit_exceeded = False

    def start_requests(self):
        if not os.path.exists(LISTCSVGZ):
            urlretrieve(LISTURL, LISTCSVGZ)
            self.logger.info(f'Retrieved {LISTCSVGZ}')

        try:
            with zipfile.ZipFile(OUTTEXTARCHIVE, "r", compression=zipfile.ZIP_DEFLATED) as zipf:
                txtset = set(zipf.namelist())
                self.logger.info(f"{OUTTEXTARCHIVE} already contains {len(zipf.infolist())} files ({len(txtset)} unique)")
        except FileNotFoundError:
            txtset = set()

        with gzip.open(LISTCSVGZ, 'rt', newline='') as csvfile:
            skipped = 0
            reader = csv.reader(csvfile, delimiter='\t')
            for ident, versions, catpos in reader:
                versions = versions_load(versions)
                if f"{ident}{versions[-1][0]}.txt" not in txtset:
                    yield self.guess_Request(ident, versions[-1][0])
                else:
                    skipped += 1
                    if skipped % 100 == 0:
                        self.logger.info(f"Skipped {skipped} files with existing .txt")

    def guess_Request(self, ident, version):
        try:
            month, subid = ident.split('.', 1)
            url = f"{URLBASE}/arxiv/arxiv/pdf/{month}/{ident}{version}.pdf"
        except ValueError:
            try:
                cat, subid = ident.split('/', 1)
                month = subid[:4]
                url = f"{URLBASE}/arxiv/{cat}/pdf/{month}/{ident}{version}.pdf"
            except ValueError:
                url = f"http://export.arxiv.org/pdf/{ident}"

        return scrapy.Request(url,
                        callback=self.parseArxivPdf,
                        cb_kwargs=dict(ident=ident, version=version),
                        errback=self.parseError)

    def parseError(self, failure):
        if failure.check(HttpError):
            if failure.request.url.startswith(URLBASE):
                cb_kwargs = failure.request.cb_kwargs
                ident = cb_kwargs["ident"]
                url = f"http://export.arxiv.org/pdf/{ident}"
                yield scrapy.Request(url,
                        callback=self.parseArxivPdf,
                        cb_kwargs=cb_kwargs)
            

    def parse(self, response):
        pass

    def parseArxivPdf(self, response, ident, version):
        try:
            text = pdf_get_str(response.body)
        except KeyboardInterrupt as e:
            raise e
        except:
            yield {
                    f"{ident}{version}": datetime.datetime.now().isoformat(timespec='seconds'),
                    f"{response.url}": repr(sys.exc_info())
                }
            return
        self.logger.info(response.url)
        if not self.pdfslimit_exceeded:
            with zipfile.ZipFile(OUTARCHIVE, "a", compression=zipfile.ZIP_DEFLATED) as zipf:
                zipf.writestr(
                        f"{ident}{version}.pdf",
                        response.body
                    )
            self.logger.info(f"{OUTARCHIVE} << {ident}{version}.pdf ({len(response.body)}B)")
            if os.stat(OUTARCHIVE).st_size > PDFSZIPLIMIT:
                self.logger.warning(f"Size of {OUTARCHIVE} exceeded limit {PDFSZIPLIMIT}")
                self.pdfslimit_exceeded = True
        with zipfile.ZipFile(OUTTEXTARCHIVE, "a", compression=zipfile.ZIP_DEFLATED) as zipf:
            zipf.writestr(
                    f"{ident}{version}.txt",
                    text
                )
        self.logger.info(f"{OUTTEXTARCHIVE} << {ident}{version}.txt ({len(text.encode('utf-8'))}B)")
        if os.stat(OUTTEXTARCHIVE).st_size > TEXTSIZELIMIT:
            self.logger.warning(f"Size of {OUTTEXTARCHIVE} exceeded limit {TEXTSIZELIMIT}")
            raise scrapy.exceptions.CloseSpider(reason="text out size exceeded limit")
