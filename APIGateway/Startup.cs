using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Ocelot.DependencyInjection;
using Ocelot.Middleware;
using System.Text;

namespace APIGateway
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.SaveToken = true;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        // todo: consider using common tokens configuration with Authorization service
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Tokens:Key"])),
                        ValidIssuer = Configuration.GetSection("Tokens:Issuer").Value,
                        ValidAudience = Configuration.GetSection("Tokens:Audience").Value
                    };
                });

            services.AddCors(options =>
            {
                options.AddPolicy("DefaultCorsPolicy",
                    builder => builder
                        .WithOrigins(Configuration.GetSection("Cors:AllowedOrigins").Get<string[]>())
                        .WithHeaders(Configuration.GetSection("Cors:AllowedHeaders").Get<string[]>())
                        .WithMethods(Configuration.GetSection("Cors:AllowedMethods").Get<string[]>())
                        .AllowCredentials()
                );
            });

            services.AddSignalR();
            services.AddOcelot();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public static void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseCors("DefaultCorsPolicy");

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/Api/Authorization/swagger/v1/swagger.json", "Authorization v1");
                    c.SwaggerEndpoint("/Api/Dataset/swagger/v1/swagger.json", "Dataset v1");
                    c.SwaggerEndpoint("/Api/Analysis/swagger/v1/swagger.json", "Analysis v1");
                });
            }

            app.UseHttpsRedirection();

            app.UseWebSockets();
            app.UseOcelot().Wait();

            app.UseRouting();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}