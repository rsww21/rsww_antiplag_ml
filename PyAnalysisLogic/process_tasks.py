#!/usr/bin/env python3
import configparser
import json
from datetime import datetime, timezone
import concurrent.futures

import pika

from pdftext import pdf_get_str
from analyse import run_analyser, build_fingerprinter

config = configparser.ConfigParser()
config.read('pyanalysis.cfg')
cfg = config['Analysis']

MAX_DOC_LENGTH = int(cfg['MaxDocumentLength'])

contents = dict() #TODO: analysed content storage
contents_order = []
contents_sumlen = 0
CONTENTS_SUMLENLIM = 1024**2 * 64

def analyse_callback(ch, method, properties, body):
    if properties.content_type == "application/octet-stream":
        return receive_file_callback(ch, method, properties, body)
    docid = properties.correlation_id
    authorid = json.loads(body)
    print(f" [analyse] doc:{docid} author:{authorid}")
    start_date = datetime.now(timezone.utc).isoformat()

    details = ""

    def build_response(percentage, detailtext):
        return json.dumps({
            'Id': docid,
            'AuthorId': authorid,
            'Date':start_date,
            'PercentageOfPlagiarism': percentage,
            'details': detailtext
        }, separators=(',', ':'))

    text = contents.get(docid)
    if text and len(build_fingerprinter().sanitize(text)) >= 25:
        first = True
        with concurrent.futures.ThreadPoolExecutor(max_workers=1) as executor:
            stop = False
            def heartbeat(connection):
                while not stop:
                    connection.process_data_events()
                    for _ in range(10):
                        connection.sleep(0.5)
                        if stop:
                            return
            future = executor.submit(heartbeat, ch.connection)
            i = 0
            for newdet, ratio in run_analyser(text):
                stop = True
                if first:
                    first = False
                else:
                    details += "\n\n"
                details += newdet
                response = build_response(ratio*100., details)
                future.result() # synchronize: ensure thread stopped
                ch.basic_publish(
                    exchange='',
                    routing_key=cfg['ReportsQueueName'],
                    body=response
                )
                i += 1
                if i < 2:
                    future = executor.submit(heartbeat, ch.connection)
    else:
        details += ("Could not analyse file, please upload it again\n"
        "and ensure it's PDF containing text (at least 25 alphanumeric characters)")
        response = build_response(0., details)
        ch.basic_publish(
            exchange='',
            routing_key=cfg['ReportsQueueName'],
            body=response
        )
    ch.basic_ack(delivery_tag=method.delivery_tag)

def receive_file_callback(ch, method, properties, body):
    task, content = body.split(b'\0', maxsplit=1)
    docid = properties.correlation_id
    task = task.decode('ascii')
    assert task == "analysis"
    try:
        text = pdf_get_str(content)
        print(f"pdf {docid} converted to text")
    except KeyboardInterrupt as e:
        raise e
    except:
        # fallback to text
        try:
            text = content.decode('utf-8')
            print(f"{docid} fallback - as text")
        except UnicodeError as e:
            print(e)
            text = ""
            print(f"{docid} failed to convert")
    if len(text) > MAX_DOC_LENGTH:
        print(f"text is too long {len(text)} and will be trimmed to {MAX_DOC_LENGTH}")
        text = text[:MAX_DOC_LENGTH]
    del content
    contents[docid] = text #TODO: storage
    contents_order.append(docid)
    global contents_sumlen
    contents_sumlen += len(text)

    ch.basic_ack(delivery_tag=method.delivery_tag)
    while len(contents_order) > 10 and contents_sumlen > CONTENTS_SUMLENLIM:
        remid = contents_order.pop(0)
        contents_sumlen -= len(contents[remid])
        del contents[remid]
        print("Dropped cached file", remid)

while True:
    try:
        cred = pika.credentials.PlainCredentials(cfg['UserName'], cfg['Password'])
        connection = pika.BlockingConnection(pika.ConnectionParameters(host=cfg['Hostname'], credentials=cred))
        channel = connection.channel()
        channel.queue_declare(queue=cfg['ReportsQueueName'])
        channel.queue_declare(queue=cfg['AnalysisQueueName'])

        channel.basic_qos(prefetch_count=1)
        #channel.basic_consume(queue=cfg['FileReceiveQueueName'], on_message_callback=receive_file_callback)
        channel.basic_consume(queue=cfg['AnalysisQueueName'], on_message_callback=analyse_callback)
        channel.start_consuming()
    # Don't recover if connection was closed by broker
    except pika.exceptions.ConnectionClosedByBroker:
        break
    # Don't recover on channel errors
    except pika.exceptions.AMQPChannelError:
        break
    # Recover on all other connection errors
    except pika.exceptions.AMQPConnectionError as e:
        print(repr(e))
        continue
