import sys
from collections import Counter
from difflib import SequenceMatcher
from itertools import groupby, chain
import string
from pprint import pformat
import os

import pyspark
import fingerprint
from fingerprint import Fingerprint, FingerprintException
#import nltk

def join_breaked_words(text):
    # possibly nltk.corpus.brown.words()
    return text.replace("-\n","")

Psplit = set(string.punctuation) - {"'"}

KGRAM_LEN = 25
WINDOW_LEN = 50

def build_fingerprinter():
    return Fingerprint(kgram_len=KGRAM_LEN, window_len=WINDOW_LEN, base=101, modulo=4294967291)

def estimate_hashes_count(text):
    fprint = build_fingerprinter()
    return (len(fprint.sanitize(text))-KGRAM_LEN)/(WINDOW_LEN/2)

def analyse_fast(record, broadcastText, THRESHOLD=30):
    hashmap, guidmap = record
    values = pyspark.TaskContext.get().partitionId(), len(hashmap), len(guidmap)
    print("Analysing on partition", *values, file=sys.stderr)
    fprint = build_fingerprinter()
    try:
        hgen = fprint.generate_hashes(fprint.sanitize(broadcastText.value))
        cnt = 0
        counts = Counter()
        results = dict()
        for h in hgen:
            sresult = hashmap.get(h)
            if sresult:
                counts.update(sresult)
            cnt += 1
        if cnt//(WINDOW_LEN//2) < THRESHOLD:
            THRESHOLD = max(1, cnt//(WINDOW_LEN//2))
        print(cnt, "threshold", THRESHOLD)
        most_common = {num for num, _ in counts.most_common(5)}

        for key, count in counts.items():
            if count >= THRESHOLD:
                guid, name = guidmap[key]
                results[guid] = name, count, True
            elif key in most_common:
                guid, name = guidmap[key]
                results[guid] = name, count, False
    except (FingerprintException, IndexError):
        results = dict()

    return results

def splitpoint(c):
    return c in Psplit

def simple_tokenize(text):
    return list(chain.from_iterable(''.join(g).lower().split() for _, g in groupby(text, key=splitpoint)))

def analyse_match(record, broadcastText):
    guid, value = record
    name, url, content = value.split('\0', maxsplit=2)
    reftext = join_breaked_words(content)
    text = broadcastText.value

    #try:
        #s, refs = map(nltk.tokenize.word_tokenize, (text, reftext))
    #except ...:
        #download
    s, refs = map(simple_tokenize, (text, reftext))
    P = set(string.punctuation)
    def notpunctuation(c):
        return len(c) != 1 or c not in P

    snp, refsnp = map(lambda l: list(filter(notpunctuation, l)), (s, refs))

    s = SequenceMatcher(None, snp, refsnp)

    ratio = s.ratio()
    LENLIM = 50
    maxmatch = 0,0,0

    ranges = [4,10,20,30]
    lastrange_sum = 0
    words_matched = 0
    counts = dict.fromkeys(ranges, 0)

    for si, refi, size in s.get_matching_blocks():
        if size > maxmatch[0]:
            maxmatch = si, refi, size
        pr = 0
        for r in ranges:
            if size < r:
                if pr:
                    counts[pr] += 1
                    if pr > 4:
                        words_matched += size
                break
            pr = r
        else:
            counts[pr] += 1
            lastrange_sum += size
    words_matched += lastrange_sum

    words_ratio = words_matched / len(snp)

    si, refi, size = maxmatch

    textmatch = refsnp[refi:refi+size] if size > 0 else []
    if size > LENLIM:
        textmatch = textmatch[:LENLIM]
        textmatch.append('...')

    return ratio, words_ratio, (size, ' '.join(textmatch), counts, lastrange_sum), guid, name


WORKERS = 4

def run_analyser(input_text, workers=WORKERS, limit=None):
    """
    This function is generator, it yields initial report and then final report

    default limit is 5*workers
    """

    limit = limit or 5*workers

    text = join_breaked_words(input_text)

    ENV = os.environ

    spark_context = pyspark.SparkContext.getOrCreate(
        pyspark.SparkConf() \
            .setMaster( "spark://10.40.71.55:7077" ) \
            .setAppName('rsww_4_analyse')
            .set( "spark.cores.max", str(WORKERS) )
            .set( "spark.executor.memory", "3584m")
            .set( "spark.driver.port", ENV.get('SPARK_DRIVER_PORT'))
            .set( "spark.ui.port", ENV.get('SPARK_UI_PORT'))
            .set( "spark.blockManager.port", ENV.get('SPARK_BLOCKMGR_PORT'))
            .set( "spark.driver.host", '10.40.71.55') # a node of swarm will always listen here
            .set( "spark.driver.bindAddress", '0.0.0.0')
    )
    try:
        spark_context.addPyFile('dependencies.zip')
        broadcastText = spark_context.broadcast(text)

        fpath = "hdfs://10.40.71.55:9000/group4/data/hashes.pkl"

        hashes = spark_context.pickleFile(fpath, 20)
        results = hashes.map(lambda r: analyse_fast(r, broadcastText)).collect()

        joined = dict()
        for r in results:
            joined.update(r)

        joined = sorted(joined.items(), key=lambda r: r[1][1], reverse=True)

        est_hash_count = estimate_hashes_count(text)

        sum_threshold = min(100, est_hash_count/10)
        summed_cnt = sum(count
                            for guid, (name, count, overThreshold) in joined
                            if count > sum_threshold
                        )
        yield "Initial similar documents:\n"+pformat(joined[:limit]), summed_cnt/est_hash_count

        bdcstQualified = spark_context.broadcast({guid for guid, params in joined[:limit]})

        fpathseq = "hdfs://10.40.71.55:9000/group4/data/partitions.seq"

        seq = spark_context.sequenceFile(fpathseq)
        matchresults = seq.filter(lambda v: v[0] in bdcstQualified.value).map(lambda r: analyse_match(r, broadcastText)).collect()
    except (Exception, GeneratorExit) as e:
        spark_context.stop()
        raise e

    spark_context.stop()
    matchresults = sorted(matchresults, key=lambda r: r[2][0], reverse=True)

    summed_words_ratio = sum(words_ratio for ratio, words_ratio, stats, guid, name in matchresults)

    yield "Fully analysed samples:\n"+pformat(matchresults), summed_words_ratio

