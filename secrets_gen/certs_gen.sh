#!/bin/sh

dotnet dev-certs https --clean && dotnet dev-certs https -ep /cert/Authorization.pfx -p "${SECRET_CERT_PASS}"
dotnet dev-certs https --clean && dotnet dev-certs https -ep /cert/APIGateway.pfx -p "${SECRET_CERT_PASS}"
dotnet dev-certs https --clean && dotnet dev-certs https -ep /cert/Dataset.pfx -p "${SECRET_CERT_PASS}"
dotnet dev-certs https --clean && dotnet dev-certs https -ep /cert/Analysis.pfx -p "${SECRET_CERT_PASS}"
