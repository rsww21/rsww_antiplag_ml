using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace Authorization
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                    webBuilder.UseSentry(options => { options.TracesSampleRate = 1.0; });
                });
        }
    }
}