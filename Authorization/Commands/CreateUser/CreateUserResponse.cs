﻿namespace Authorization.Commands.CreateUser
{
    public class CreateUserResponse
    {
        public CreateUserResponse(string id, string userName)
        {
            Id = id;
            UserName = userName;
        }

        public string Id { get; }
        public string UserName { get; }
    }
}