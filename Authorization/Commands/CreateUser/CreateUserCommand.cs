﻿using Authorization.Common.Exceptions;
using Authorization.Common.Models;
using Authorization.Data.Entities;
using MediatR;
using Microsoft.AspNetCore.Identity;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Authorization.Commands.CreateUser
{
    public class CreateUserCommand : IRequest<CreateUserResponse>
    {
        private readonly SignupModel model;

        public CreateUserCommand(SignupModel model)
        {
            this.model = model;
        }

        public class CreateUserCommandHandler : IRequestHandler<CreateUserCommand, CreateUserResponse>
        {
            private readonly UserManager<User> userManager;

            public CreateUserCommandHandler(UserManager<User> userManager)
            {
                this.userManager = userManager;
            }

            public async Task<CreateUserResponse> Handle(CreateUserCommand request, CancellationToken cancellationToken)
            {
                var requestModel = request.model;

                if (await userManager.FindByNameAsync(requestModel.UserName) != null)
                    throw new UserAlreadyExistException();

                var user = new User
                {
                    UserName = request.model.UserName,
                    SecurityStamp = Guid.NewGuid().ToString()
                };

                var result = await userManager.CreateAsync(user, requestModel.Password);
                if (!result.Succeeded) throw new UserCreationFailedException();

                return new CreateUserResponse(user.Id, user.UserName);
            }
        }
    }
}