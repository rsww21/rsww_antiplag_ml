﻿using Authorization.Common.Configurations;
using Authorization.Common.Exceptions;
using Authorization.Common.Models;
using Authorization.Data.Entities;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Authorization.Commands.Login
{
    public class LoginCommand : IRequest<LoginResponse>
    {
        private readonly LoginModel credentials;

        public LoginCommand(LoginModel credentials)
        {
            this.credentials = credentials;
        }

        public class LoginCommandHandler : IRequestHandler<LoginCommand, LoginResponse>
        {
            private readonly JwtConfiguration jwtConfig;
            private readonly SignInManager<User> signInManager;
            private readonly UserManager<User> userManager;

            public LoginCommandHandler(SignInManager<User> signInManager, UserManager<User> userManager,
                JwtConfiguration jwtConfig)
            {
                this.signInManager = signInManager;
                this.userManager = userManager;
                this.jwtConfig = jwtConfig;
            }

            public async Task<LoginResponse> Handle(LoginCommand request, CancellationToken cancellationToken)
            {
                var user = await userManager.FindByNameAsync(request.credentials.UserName);
                if (user == null) throw new UserNotFoundException();

                var passwordCheck =
                    await signInManager.CheckPasswordSignInAsync(user, request.credentials.Password, false);

                if (!passwordCheck.Succeeded) throw new UserNotFoundException();

                var claims = new List<Claim>
                {
                    new(JwtRegisteredClaimNames.Sub, user.Id),
                    new("UserId", user.Id),
                    new(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    new(JwtRegisteredClaimNames.UniqueName, user.UserName)
                };

                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtConfig.Key));
                var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(claims),
                    Expires = DateTime.UtcNow.AddHours(3),
                    SigningCredentials = credentials,
                    Issuer = jwtConfig.Issuer,
                    Audience = jwtConfig.Audience
                };

                var tokenHandler = new JwtSecurityTokenHandler();
                var token = tokenHandler.CreateToken(tokenDescriptor);
                return new LoginResponse(tokenHandler.WriteToken(token), token.ValidTo);
            }
        }
    }
}