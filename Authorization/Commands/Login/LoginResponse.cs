﻿using System;

namespace Authorization.Commands.Login
{
    public class LoginResponse
    {
        public LoginResponse(string token, DateTime validTo)
        {
            Token = token;
            ValidTo = validTo;
        }

        public string Token { get; }
        public DateTime ValidTo { get; }
    }
}