﻿using System.ComponentModel.DataAnnotations;

namespace Authorization.Common.Models
{
    public class SignupModel
    {
        [Required] public string UserName { get; set; }

        [Required] public string Password { get; set; }
    }
}