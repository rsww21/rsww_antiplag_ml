﻿using Authorization.Commands.CreateUser;
using Authorization.Commands.Login;
using Authorization.Common.Exceptions;
using Authorization.Common.Models;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Authorization.Controllers
{
    [ApiController]
    [Route("Api/[controller]")]
    public class AuthorizationController : ControllerBase
    {
        private readonly IMediator mediator;

        public AuthorizationController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpPost("signup")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<CreateUserResponse>> SignupAsync([FromBody] SignupModel model)
        {
            try
            {
                var user = await mediator.Send(new CreateUserCommand(model));
                return Ok(user);
            }
            catch (UserAlreadyExistException)
            {
                return BadRequest("User already exist.");
            }
            catch (UserCreationFailedException)
            {
                return BadRequest("Error occured"); // todo
            }
        }

        [HttpPost("login")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<LoginResponse>> LoginAsync([FromBody] LoginModel model)
        {
            try
            {
                var token = await mediator.Send(new LoginCommand(model));
                return Ok(token);
            }
            catch (UserNotFoundException)
            {
                return BadRequest("Invalid login and/or password.");
            }
        }
    }
}
