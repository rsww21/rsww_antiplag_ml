﻿using Authorization.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace Authorization.Data
{
    public class UsersContext : DbContext
    {
        public UsersContext(DbContextOptions<UsersContext> options) : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
    }
}