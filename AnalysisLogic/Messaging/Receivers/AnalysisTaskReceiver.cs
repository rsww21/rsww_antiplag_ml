using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AnalysisLogic.Messaging.Configuration;
using AnalysisLogic.Messaging.Senders;
using AnalysisLogic.Models;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace AnalysisLogic.Messaging.Receivers
{
    public class AnalysisTaskReceiver : BackgroundService
    {
        private readonly RabbitMqConfiguration rabbitMqConfig;
        private readonly IReportSender reportSender;
        private IModel channel;
        private IConnection connection;

        public AnalysisTaskReceiver(RabbitMqConfiguration rabbitMqConfig, IReportSender reportSender)
        {
            this.rabbitMqConfig = rabbitMqConfig;
            this.reportSender = reportSender;
            InitializeRabbitMqListener();
        }

        private void InitializeRabbitMqListener()
        {
            var factory = new ConnectionFactory
            {
                HostName = rabbitMqConfig.Hostname,
                UserName = rabbitMqConfig.UserName,
                Password = rabbitMqConfig.Password
            };

            connection = factory.CreateConnection();
            connection.ConnectionShutdown += RabbitMQ_ConnectionShutdown;
            channel = connection.CreateModel();
            channel.QueueDeclare(rabbitMqConfig.AnalysisQueueName, false, false, false, null);
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            stoppingToken.ThrowIfCancellationRequested();

            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (ch, ea) =>
            {
                var docId = ea.BasicProperties.CorrelationId;

                var content = Encoding.UTF8.GetString(ea.Body.ToArray());
                var authorId = JsonConvert.DeserializeObject<Guid>(content);

                HandleMessage(new Guid(docId), authorId);

                channel.BasicAck(ea.DeliveryTag, false);
            };
            consumer.Shutdown += OnConsumerShutdown;
            consumer.Registered += OnConsumerRegistered;
            consumer.Unregistered += OnConsumerUnregistered;
            consumer.ConsumerCancelled += OnConsumerConsumerCancelled;

            channel.BasicConsume(rabbitMqConfig.AnalysisQueueName, false, consumer);

            return Task.CompletedTask;
        }

        private void HandleMessage(Guid docId, Guid authorId) // todo: move this to other class
        {
            Thread.Sleep(3000); // simulate processing
            var report = new Report
            {
                Id = docId,
                AuthorId = authorId,
                Date = DateTime.Now,
                PercentageOfPlagiarism = new Random().NextDouble() * 100
            };

            reportSender.SendReport(report);
        }

        private void OnConsumerConsumerCancelled(object sender, ConsumerEventArgs e)
        {
        }

        private void OnConsumerUnregistered(object sender, ConsumerEventArgs e)
        {
        }

        private void OnConsumerRegistered(object sender, ConsumerEventArgs e)
        {
        }

        private void OnConsumerShutdown(object sender, ShutdownEventArgs e)
        {
        }

        private void RabbitMQ_ConnectionShutdown(object sender, ShutdownEventArgs e)
        {
        }

        public override void Dispose()
        {
            channel.Close();
            connection.Close();
            base.Dispose();
        }
    }
}