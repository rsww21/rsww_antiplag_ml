namespace AnalysisLogic.Messaging.Configuration
{
    public class RabbitMqConfiguration
    {
        public string Hostname { get; set; }
        public string ReportsQueueName { get; set; }
        public string AnalysisQueueName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}