using AnalysisLogic.Models;

namespace AnalysisLogic.Messaging.Senders
{
    public interface IReportSender
    {
        void SendReport(Report report);
    }
}