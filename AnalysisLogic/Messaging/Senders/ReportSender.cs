using System.Text;
using AnalysisLogic.Messaging.Configuration;
using AnalysisLogic.Models;
using Newtonsoft.Json;
using RabbitMQ.Client;

namespace AnalysisLogic.Messaging.Senders
{
    public class ReportSender : IReportSender
    {
        private readonly RabbitMqConfiguration rabbitMqConfig;
        private IConnection connection;

        public ReportSender(RabbitMqConfiguration rabbitMqConfig)
        {
            this.rabbitMqConfig = rabbitMqConfig;
        }

        public void SendReport(Report report)
        {
            if (!ConnectionExists()) return;

            using var channel = connection.CreateModel();
            channel.QueueDeclare(rabbitMqConfig.ReportsQueueName, false, false, false, null);
            var props = channel.CreateBasicProperties();
            props.CorrelationId = report.Id.ToString();

            var json = JsonConvert.SerializeObject(report);
            var body = Encoding.UTF8.GetBytes(json);

            channel.BasicPublish("", rabbitMqConfig.ReportsQueueName, props, body);
        }

        private void CreateConnection()
        {
            var factory = new ConnectionFactory
            {
                HostName = rabbitMqConfig.Hostname,
                UserName = rabbitMqConfig.UserName,
                Password = rabbitMqConfig.Password
            };
            connection = factory.CreateConnection();
        }

        private bool ConnectionExists()
        {
            if (connection != null) return true;

            CreateConnection();

            return connection != null;
        }
    }
}