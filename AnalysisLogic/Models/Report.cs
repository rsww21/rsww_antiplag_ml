﻿using System;

namespace AnalysisLogic.Models
{
    public class Report
    {
        public Guid Id { get; set; }
        public Guid AuthorId { get; set; }
        public DateTime Date { get; set; }
        public double PercentageOfPlagiarism { get; set; }
    }
}