using AnalysisLogic.Messaging.Configuration;
using AnalysisLogic.Messaging.Receivers;
using AnalysisLogic.Messaging.Senders;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Sentry;

namespace AnalysisLogic
{
    public class Program
    {
        public static void Main(string[] args)
        {
            using (SentrySdk.Init("https://2cbbd2762349417aa402cbd3a5e16395@o724463.ingest.sentry.io/5782317"))
            {
                CreateHostBuilder(args).Build().Run();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((builderContext, services) =>
                {
                    services.Configure<RabbitMqConfiguration>(builderContext.Configuration.GetSection("RabbitMq"));
                    services.AddSingleton(sp => sp.GetRequiredService<IOptions<RabbitMqConfiguration>>().Value);

                    services.AddSingleton<IReportSender, ReportSender>();
                    services.AddHostedService<AnalysisTaskReceiver>();
                });
    }
}