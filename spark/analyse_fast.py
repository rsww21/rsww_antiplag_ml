import sys
from collections import Counter

import pyspark
import fingerprint
from fingerprint import Fingerprint, FingerprintException

def join_breaked_words(text):
    # possibly nltk.corpus.brown.words()
    return text #TODO

with open(sys.argv[1]) as analysed_file:
    text = join_breaked_words(analysed_file.read())

spark_context = pyspark.SparkContext.getOrCreate(
    pyspark.SparkConf() \
        .setMaster( "spark://10.40.71.55:7077" ) \
        .setAppName('rsww_4_analyse_f')
        .set( "spark.cores.max", "4" )
        .set( "spark.executor.memory", "3g")
)

spark_context.addPyFile('dependencies.zip')
broadcastText = spark_context.broadcast(text)

#spark_session = pyspark.sql.SparkSession( spark_context )

fpath = "hdfs://10.40.71.55:9000/group4/data/hashes.pkl"

def analyse_fast(record, THRESHOLD=30):
    hashmap, guidmap = record
    values = pyspark.TaskContext.get().partitionId(), len(hashmap), len(guidmap)
    print("Analysing on partition", *values, file=sys.stderr)
    KGRAM_LEN = 25
    WINDOW_LEN = 50
    fprint = Fingerprint(kgram_len=KGRAM_LEN, window_len=50, base=101, modulo=4294967291)
    try:
        hgen = fprint.generate_hashes(fprint.sanitize(broadcastText.value))
        cnt = 0
        counts = Counter()
        results = dict()
        for h in hgen:
            sresult = hashmap.get(h)
            if sresult:
                counts.update(sresult)
            cnt += 1
        if cnt//(WINDOW_LEN//2) < THRESHOLD:
            THRESHOLD = max(1, cnt//(WINDOW_LEN//2))
        print(cnt, "threshold", THRESHOLD)
        most_common = {num for num, _ in counts.most_common(5)}

        for key, count in counts.items():
            if count >= THRESHOLD:
                guid, name = guidmap[key]
                results[guid] = name, count, True
            elif key in most_common:
                guid, name = guidmap[key]
                results[guid] = name, count, False
    except (FingerprintException, IndexError):
        results = dict()

    return results

hashes = spark_context.pickleFile(fpath, 8)
results = hashes.map(analyse_fast).collect()

spark_context.stop()

from pprint import pprint
joined = dict()
for r in results:
    joined.update(r)

joined = sorted(joined.items(), key=lambda r: r[1][1], reverse=True)

pprint(joined[:10])

import pickle
with open('results.pkl', 'wb') as f:
    pickle.dump(joined, f)
