import pickle

import pyspark
import fingerprint
from fingerprint import Fingerprint, FingerprintException

spark_context = pyspark.SparkContext.getOrCreate(
    pyspark.SparkConf() \
        .setMaster( "spark://10.40.71.55:7077" ) \
        .setAppName('rsww_4_fingerprint')
        .set( "spark.cores.max", "4" )
        .set( "spark.executor.memory", "3g")
)

print(fingerprint.__package__)
spark_context.addPyFile('dependencies.zip')


fpath = "hdfs://10.40.71.55:9000/group4/data/partitions.seq"

def produce_hashes(record):
    guid, value = record
    name, url, content = value.split('\0', maxsplit=2)
    fprint = Fingerprint(kgram_len=25, window_len=50, base=101, modulo=4294967291)
    try:
        h = fprint.generate(content)
    except (FingerprintException, IndexError):
        h = []
    hset = {hash_ for hash_, pos in h}
    return (guid, hset, name)


def merge_hashes(hashmap, hashes, num):
    for h in hashes:
        hashmap.setdefault(h, []).append(num)

def make_dicts(resultlist):
    hashmap = dict()
    guidmap = dict()
    num = 0

    for guid, values, name in resultlist:
        guidmap[num] = guid, name
        merge_hashes(hashmap, values, num)
        num += 1

    print('hashes:', len(hashmap), 'documents:', len(guidmap))
    return hashmap, guidmap

seq = spark_context.sequenceFile(fpath)
hashes = seq.map(produce_hashes).repartition(20).glom().map(make_dicts).saveAsPickleFile("hdfs://10.40.71.55:9000/group4/data/hashes.pkl")

spark_context.stop()

