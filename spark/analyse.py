import sys
from collections import Counter
from difflib import SequenceMatcher
from itertools import groupby, chain
import string

import pyspark
import fingerprint
from fingerprint import Fingerprint, FingerprintException
#import nltk

def join_breaked_words(text):
    # possibly nltk.corpus.brown.words()
    return text.replace("-\n","")

WORKERS = 4

with open(sys.argv[1]) as analysed_file:
    text = join_breaked_words(analysed_file.read())

spark_context = pyspark.SparkContext.getOrCreate(
    pyspark.SparkConf() \
        .setMaster( "spark://10.40.71.55:7077" ) \
        .setAppName('rsww_4_analyse')
        .set( "spark.cores.max", str(WORKERS) )
        .set( "spark.executor.memory", "3584m")
)

spark_context.addPyFile('dependencies.zip')
broadcastText = spark_context.broadcast(text)

#spark_session = pyspark.sql.SparkSession( spark_context )

fpath = "hdfs://10.40.71.55:9000/group4/data/hashes.pkl"

def analyse_fast(record, THRESHOLD=30):
    hashmap, guidmap = record
    values = pyspark.TaskContext.get().partitionId(), len(hashmap), len(guidmap)
    print("Analysing on partition", *values, file=sys.stderr)
    KGRAM_LEN = 25
    WINDOW_LEN = 50
    fprint = Fingerprint(kgram_len=KGRAM_LEN, window_len=WINDOW_LEN, base=101, modulo=4294967291)
    try:
        hgen = fprint.generate_hashes(fprint.sanitize(broadcastText.value))
        cnt = 0
        counts = Counter()
        results = dict()
        for h in hgen:
            sresult = hashmap.get(h)
            if sresult:
                counts.update(sresult)
            cnt += 1
        if cnt//(WINDOW_LEN//2) < THRESHOLD:
            THRESHOLD = max(1, cnt//(WINDOW_LEN//2))
        print(cnt, "threshold", THRESHOLD)
        most_common = {num for num, _ in counts.most_common(5)}

        for key, count in counts.items():
            if count >= THRESHOLD:
                guid, name = guidmap[key]
                results[guid] = name, count, True
            elif key in most_common:
                guid, name = guidmap[key]
                results[guid] = name, count, False
    except (FingerprintException, IndexError):
        results = dict()

    return results

hashes = spark_context.pickleFile(fpath, 20)
results = hashes.map(analyse_fast).collect()


from pprint import pprint
joined = dict()
for r in results:
    joined.update(r)

joined = sorted(joined.items(), key=lambda r: r[1][1], reverse=True)

pprint(joined[:15])

LIMIT = 5*WORKERS

bdcstQualified = spark_context.broadcast({guid for guid, params in joined[:LIMIT]})


Psplit = set(string.punctuation) - {"'"}
def splitpoint(c):
    return c in Psplit

def simple_tokenize(text):
    return list(chain.from_iterable(''.join(g).lower().split() for _, g in groupby(text, key=splitpoint)))

def analyse_match(record):
    guid, value = record
    name, url, content = value.split('\0', maxsplit=2)
    reftext = join_breaked_words(content)

    #try:
        #s, refs = map(nltk.tokenize.word_tokenize, (text, reftext))
    #except ...:
        #download
    s, refs = map(simple_tokenize, (text, reftext))
    #print(s[10:20])
    #print(refs[:50])
    P = set(string.punctuation)
    def notpunctuation(c):
        return len(c) != 1 or c not in P

    snp, refsnp = map(lambda l: list(filter(notpunctuation, l)), (s, refs))

    s = SequenceMatcher(None, snp, refsnp)
    
    ratio = s.ratio()
    LENLIM = 50
    maxmatch = 0,0,0
    
    ranges = [4,10,20,30]
    counts = dict.fromkeys(ranges, 0)
    
    for si, refi, size in s.get_matching_blocks():
        if size > maxmatch[0]:
            maxmatch = si, refi, size
        pr = 0
        for r in ranges:
            if size < r:
                if pr:
                    counts[pr] += 1
                break
            pr = r
        else:
            counts[pr] += 1

    si, refi, size = maxmatch
    
    textmatch = refsnp[refi:refi+size] if size > 0 else []
    if size > LENLIM:
        textmatch = textmatch[:LENLIM]
        textmatch.append('...')

    return ratio, (size, ' '.join(textmatch), counts), guid, name

fpathseq = "hdfs://10.40.71.55:9000/group4/data/partitions.seq"

seq = spark_context.sequenceFile(fpathseq)
matchresults = seq.filter(lambda v: v[0] in bdcstQualified.value).map(analyse_match).collect()

spark_context.stop()

matchresults = sorted(matchresults, key=lambda r: r[1][0], reverse=True)

print(len(matchresults))
pprint(matchresults[:15])

import pickle
with open('results.pkl', 'wb') as f:
    pickle.dump(joined, f)
    pickle.dump(matchresults, f)
