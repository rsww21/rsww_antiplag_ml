#!/bin/bash
PYPATH=$1
[ -n "${PYPATH}" ] || PYPATH="../env/lib/python3.8/site-packages/"

python3 -m pip install -r requirements.txt
python3 -m zipfile -c dependencies.zip "${PYPATH}/fingerprint/" # "${PYPATH}/nltk/" "${PYPATH}/click/" "${PYPATH}/regex/" "${PYPATH}/joblib/" "${PYPATH}/tqdm/"
