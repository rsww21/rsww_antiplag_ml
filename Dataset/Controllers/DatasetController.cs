﻿using Dataset.Commands.PostDocument;
using Dataset.Common.Exceptions;
using Dataset.Common.Models;
using Dataset.Queries.GetDocuments;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Threading.Tasks;

namespace Dataset.Controllers
{
    [ApiController]
    [Route("Api/[controller]")]
    public class DatasetController : ControllerBase
    {
        private readonly IMediator mediator;

        public DatasetController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<GetDocumentsResponse>> Get([FromQuery] uint page)
        {
            try
            {
                var documents = await mediator.Send(new GetDocumentsQuery(page));
                return Ok(documents);
            }
            catch (Exception)
            {
                return BadRequest("Error occured"); // todo
            }
        }


        const long MAXFILESIZE = 28 * (1 << 20); // 28 MB

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<PostDocumentResponse>> Post([FromForm, Required] string name, [Required] IFormFile file, [FromForm, Url] string srcurl)
        {
            try
            {
                if (file.Length <= MAXFILESIZE)
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        await file.CopyToAsync(memoryStream);
                        var model = new PostDocumentModel
                        {
                            Name = name,
                            Content = memoryStream.ToArray(),
                            Srcurl = srcurl
                        };
                        var documents = await mediator.Send(new PostDocumentCommand(model));
                        return Ok(documents);
                    }
                }
                else
                {
                    throw new FileTooLargeException();
                }
            }
            catch (FileTooLargeException)
            {
                return BadRequest("File too large");
            }
            //catch (Exception)
            //{
            //    return BadRequest("Error occured"); // todo
            //}
        }
    }
}
