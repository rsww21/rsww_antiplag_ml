using Dataset.Common;
//using Dataset.Common.Configurations;
//using Dataset.Common.Exceptions;
using Dataset.Common.Models;
using MediatR;
using StackExchange.Redis;
using System;
//using System.IdentityModel.Tokens.Jwt;
using System.Threading;
using System.Threading.Tasks;

namespace Dataset.Commands.PostDocument
{
    public class PostDocumentCommand : IRequest<PostDocumentResponse>
    {
        private readonly PostDocumentModel document;

        public PostDocumentCommand(PostDocumentModel document)
        {
            this.document = document;
        }

        public class PostDocumentCommandHandler : IRequestHandler<PostDocumentCommand, PostDocumentResponse>
        {
            private readonly IRedisConnection redis;

            public PostDocumentCommandHandler(IRedisConnection redis)
            {
                this.redis = redis;
            }

            public async Task<PostDocumentResponse> Handle(PostDocumentCommand request, CancellationToken cancellationToken)
            {
                IDatabase database = redis.GetDatabase();
                var key = Guid.NewGuid().ToString();
                var a1 = database.ListLeftPushAsync("Dataset", key);
                var a2 = database.StringSetAsync(key, request.document.Name);
                await Task.WhenAll(a1, a2);
                return new PostDocumentResponse(key, request.document.Name);
            }
        }
    }
}
