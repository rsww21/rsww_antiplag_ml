using System.ComponentModel.DataAnnotations;

namespace Dataset.Common.Models
{
    public class DocumentModel
    {
        public DocumentModel(string name, string id, string srcurl = null)
        {
            Name = name;
            Id = id;
            Srcurl = srcurl;
        }

        [Required] public string Name { get; }

        [Required] public string Id { get; }

        [Url] public string Srcurl { get; }
    }
}