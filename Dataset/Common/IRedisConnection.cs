using StackExchange.Redis;
namespace Dataset.Common
{
    public interface IRedisConnection
    {
        public ConnectionMultiplexer Connection { get; }

        public IDatabase GetDatabase();

        public System.Net.EndPoint[] GetEndPoints();

        public IServer GetServer(string host, int port);
    }
}