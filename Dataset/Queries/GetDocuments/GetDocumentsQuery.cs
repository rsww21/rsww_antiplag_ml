using Dataset.Common;
//using Dataset.Common.Configurations;
//using Dataset.Common.Exceptions;
using Dataset.Common.Models;
using MediatR;
using StackExchange.Redis;
using System.Collections.Generic;
using System.Linq;
//using System.IdentityModel.Tokens.Jwt;
using System.Threading;
using System.Threading.Tasks;

namespace Dataset.Queries.GetDocuments
{
    public class GetDocumentsQuery : IRequest<GetDocumentsResponse>
    {
        private readonly uint Page;

        const uint PAGESIZE = 20;

        public GetDocumentsQuery(uint page = 0)
        {
            this.Page = page;
        }

        public class GetDocumentsQueryHandler : IRequestHandler<GetDocumentsQuery, GetDocumentsResponse>
        {
            private readonly IRedisConnection redis;

            public GetDocumentsQueryHandler(IRedisConnection redis)
            {
                this.redis = redis;
            }

            public async Task<GetDocumentsResponse> Handle(GetDocumentsQuery request, CancellationToken cancellationToken)
            {
                long start = request.Page * PAGESIZE;
                long stop = start + PAGESIZE - 1;
                IDatabase database = redis.GetDatabase();
                var lengthTask = database.ListLengthAsync("Dataset");
                var listTask = database.ListRangeAsync("Dataset", start, stop);
                long listLength = await lengthTask;
                long pagesLeft = listLength == 0 ? 0 :
                                    (listLength + PAGESIZE - 1) / PAGESIZE - request.Page - 1;

                RedisValue[] documentIds = await listTask;
                List<Task<RedisValue>> namesTasks = new List<Task<RedisValue>>();

                foreach (var key in documentIds)
                {
                    namesTasks.Add(database.StringGetAsync(key.ToString()));
                }
                RedisValue[] names = await Task.WhenAll(namesTasks);

                return new GetDocumentsResponse(
                    documentIds.Zip(names,
                        (id, name) => new DocumentModel(name.ToString(), id.ToString())
                        ), pagesLeft);
            }
        }
    }
}
