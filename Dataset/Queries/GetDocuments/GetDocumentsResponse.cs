using Dataset.Common.Models;
using System.Collections.Generic;


namespace Dataset.Queries.GetDocuments
{
    public class GetDocumentsResponse
    {

        public GetDocumentsResponse(IEnumerable<DocumentModel> documents, long pagesLeft)
        {
            this.Documents = documents;
            this.PagesLeft = pagesLeft;
        }

        public IEnumerable<DocumentModel> Documents { get; }

        public long PagesLeft { get; }
    }
}
