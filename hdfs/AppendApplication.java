import py4j.GatewayServer;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.ArrayWritable;
// import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.SequenceFile.Writer;
import org.apache.hadoop.io.SequenceFile.Writer.Option;
import org.apache.hadoop.io.SequenceFile.CompressionType;
// import org.apache.hadoop.io.compress.DefaultCodec;
// import org.apache.hadoop.fs.FileContext;
import org.apache.hadoop.conf.Configuration;
// import org.apache.hadoop.fs.CreateFlag;
import org.apache.hadoop.fs.Path;
//import org.apache.hadoop.fs.UnsupportedFileSystemException;


public class AppendApplication {
//     public SequenceFile.Writer old_getSequenceWriter(String path) throws UnsupportedFileSystemException, java.io.IOException {
//         Path p = new Path(path);
//         FileContext ctx = FileContext.getFileContext();
//         return SequenceFile.createWriter(ctx, new Configuration(), p,
//                                             IntWritable.class,
//                                             Text.class,
//                                             SequenceFile.CompressionType.NONE,
//                                             new DefaultCodec(),
//                                             new SequenceFile.Metadata(),
//                                             java.util.EnumSet.of(CreateFlag.APPEND)
//                                         );
//     }
    
    public WriterWrapper getWriterWrapper(String parent, String fname) throws java.io.IOException {
        return new WriterWrapper(parent, fname);
    }

    public static void main(String[] args) {
        AppendApplication app = new AppendApplication();
        GatewayServer server = new GatewayServer(app);
        server.start();
    }
    
    public class WriterWrapper {
        protected final SequenceFile.Writer writer;
        public WriterWrapper(String parent, String fname) throws java.io.IOException {
            Configuration conf = new Configuration();
            Path file = new Path(parent, fname);

            Option compressOption = Writer.compression(CompressionType.NONE);
            writer =
                SequenceFile.createWriter(conf, SequenceFile.Writer.file(file),
                    SequenceFile.Writer.keyClass(Text.class),
                    SequenceFile.Writer.valueClass(Text.class),
                    SequenceFile.Writer.appendIfExists(true), compressOption);
        }

        public void append(String key, String name, String url, String text) throws java.io.IOException {
            writer.append(new Text(key), new Text(format(name, url, text)));
        }
        
        public void close() throws java.io.IOException {
            writer.close();
        }
        
        private String format(String name, String url, String text) {
            String name_esc = name.replace('\0', '?');
            String url_esc = url.replace('\0', '?');
            return name_esc + "\0" + url_esc + "\0" + text;
        }
    }
}
